# My Dotfiles

[chezmoi](https://www.chezmoi.to) to manage my dotfiles across multiple
diverse machines, securely.

## Installation

To install chezmoi and set everything up in a single command, run the following
command:

> From a terminal
```bash
sh -c "$(curl -fsLS get.chezmoi.io)" -- init --apply https://gitlab.com/azoll/dotfiles
```

## Making changes

1. Update your respective .dotfiles and add files with `chezmoi add <file/dir name>`
2. Check out your diff `chezmoi diff`
3. Apply with `chezmoi apply`
4. Commit your changes to git and push to the respository


